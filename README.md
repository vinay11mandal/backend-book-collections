# Collections Of Book
#### Tools and technologies
Language: Python 3.6
Framework: Django 2.2
Database: Postgres 11.5 (RDS)
API: Django rest framework
Code Repository : Gitlab
Continuous integration: Gitlab CI
API test tool: Swagger
Containerization: Docker

#### Install Docker into ubuntu
```
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt-get update
apt-cache policy docker-ce
sudo apt-get install -y docker-ce
sudo systemctl status docker
```

#### Application run  into local.
1. Take a clone of the repo using https.
```
git clone https://gitlab.com/vinay11mandal/backend-book-collections.git
```

2. Build docker image.
```
docker build -f Dockerfile -t book_collections_img:latest .
```
3. Run docker image.
```
docker run -p 8000:8000 book_collections_img:latest
```
4. Open URL into the browser.
```
http://0.0.0.0:8000
```
5. API test and doc's
```
http://0.0.0.0:8000/swagger/
http://0.0.0.0:8000/redoc/
```

Deploy Application on Server.
1. First push the code into the gitlab.
2. Once gitlab CI is successfully done.
3. Login into the AWS ec2 instance.
4. Docker login. using
```
docker login registry.gitlab.com
```
5. docker image pull with below commond.
```
docker pull registry.gitlab.com/vinay11mandal/backend-book-collections/book_collections:latest
```
6. Run docker image.
```
docker run -p 8000:8000 registry.gitlab.com/vinay11mandal/backend-book-collections/book_collections:latest &
```
7. Server url.
```
server_id_address:8000
```
