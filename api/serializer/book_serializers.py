from rest_framework import serializers
from django.db import transaction

from book.models import Book, Publisher, Author


class AuthorSerializer(serializers.ModelSerializer):
    """Model Serializer for Auther with specific fields"""
    class Meta:
        model = Author
        fields = [
            'id',
            'name',
            'description',
            'registration_number',
            'created',
            'modified'
        ]
        read_only_fields = ['id', 'created', 'modified']


class PublisherSerializer(serializers.ModelSerializer):
    """Model Serializer for Publisher with specific fields"""
    class Meta:
        model = Author
        fields = [
            'id',
            'name',
            'registration_number',
            'created',
            'modified'
        ]
        read_only_fields = ['id', 'created', 'modified']


class BookListSerializer(serializers.ModelSerializer):
    """Model Serializer for Book with specific fields, Author and publisher"""
    publisher = PublisherSerializer()
    author = AuthorSerializer(many=True)

    class Meta:
        model = Book
        fields = [
            'id',
            'title',
            'tag_line',
            'author',
            'publisher',
            'isbn',
            'published_at',
            'category',
            'price',
            'overview',
            'created',
            'modified'
        ]
        read_only_fields = ['id', 'created', 'modified']

    def create(self, validated_data):
        with transaction.atomic():
            publisher_data = validated_data.pop('publisher')
            authors = validated_data.pop('author')

            publisher, _ = Publisher.objects.get_or_create(**publisher_data)
            validated_data.update({'publisher': publisher})
            book = Book.objects.create(**validated_data)

            author_instance = []
            for data in authors:
                instance, _ = Author.objects.get_or_create(**data)
                book.author.add(instance)
        return book


class BookSerializer(serializers.ModelSerializer):
    """Model Serializer for Book"""

    class Meta:
        model = Book
        fields = [
            'id',
            'title',
            'tag_line',
            'isbn',
            'published_at',
            'category',
            'price',
            'overview',
            'created',
            'modified'
        ]
        read_only_fields = ['id', 'created', 'modified']
