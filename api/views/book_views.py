from django.shortcuts import render
from rest_framework import viewsets
from book.models import (Book, Publisher, Author)
from rest_framework.response import Response
from rest_framework import status
from api.serializer.book_serializers import (BookSerializer,
    PublisherSerializer, AuthorSerializer, BookListSerializer)


class BookViewSet(viewsets.ModelViewSet):
    """
    API to add the Book with Auther and Pulisher
    """
    serializer_class = BookSerializer
    queryset = Book.objects.all()

    def get_serializer_class(self):
        if self.action in ['list', 'create', 'retrieve']:
            return BookListSerializer
        else:
            return BookSerializer

    def create(self, request):
        """Post method"""
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid(raise_exception=True):
            book = serializer.save()

            return Response({
                'data' : self.serializer_class(book).data,
                'message': 'Book with Author and Publisher is added successfully.',
            }, status.HTTP_201_CREATED,)


class AutherViewSet(viewsets.ModelViewSet):
    """
        API to add the Author
    """
    serializer_class = AuthorSerializer
    queryset = Author.objects.all()


class PublisherViewSet(viewsets.ModelViewSet):
    """
        API to add the Publisher
    """
    serializer_class = PublisherSerializer
    queryset = Publisher.objects.all()
